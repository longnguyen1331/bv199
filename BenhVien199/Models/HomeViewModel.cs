using BV199.Application.ExternalServices.DrCare.Response;

namespace BenhVien199.Models
{
    public class HomeViewModel
    {
        public List<BannerRespone>? Banners { get; set; }
        public List<ServiceTypeRespone>? ServiceTypes { get; set; }
        public List<DoctorRespone>? Doctors { get; set; }
        public List<TopServiceRespone>? TopServices { get; set; }
        public List<MenuWebInfoRespone>? Menus { get; set; }
    }
}