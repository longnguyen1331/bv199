﻿using BenhVien199.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using BV199.Application.WeatherForecasts.Queries.GetCurrentBannerQuery;
using BV199.Application.WeatherForecasts.Queries.GetServiceTypeQuery;
using BV199.Application.WeatherForecasts.Queries.GetDoctorQuery;
using Autofac.Core;
using BV199.Application.WeatherForecasts.Queries.GetTopServiceQuery;
using BV199.Application.WeatherForecasts.Queries.GetWebInfoMenuQuery;

namespace BenhVien199.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index(CancellationToken cancellationToken)
        {
            var banners = await Mediator.Send(new GetCurrentBannerQuery() { }, cancellationToken);
            var serviceTypes = await Mediator.Send(new GetServiceTypeQuery() { }, cancellationToken);
            var doctors = await Mediator.Send(new GetDoctorQuery() { }, cancellationToken);
            var topServices = await Mediator.Send(new GetTopServiceQuery() { }, cancellationToken);
            var menus = await Mediator.Send(new GetWebInfoMenuQuery() { }, cancellationToken);
            //top news 
            var model = new HomeViewModel
            {
                Banners = banners.Data.data,
                ServiceTypes = serviceTypes.Data.data,
                Doctors = doctors.Data?.data,
                TopServices = topServices.Data.data,
                Menus = menus.Data.data,
            };

            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}