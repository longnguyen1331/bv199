﻿using System.Threading;
using System.Threading.Tasks;
using BV199.Application.Common.Interfaces;
using BV199.Application.Common.Mapping;
using BV199.Application.Common.Models;
using BV199.Application.ExternalServices.OpenWeather.Request;
using BV199.Application.ExternalServices.OpenWeather.Response;
using BV199.Domain.Enums;

namespace BV199.Infrastructure.Services
{
    public class OpenWeatherService : IOpenWeatherService
    {
        private readonly IHttpClientHandler _httpClient;

        private string ClientApi { get; } = "open-weather-api";

        public OpenWeatherService(IHttpClientHandler httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<ServiceResult<OpenWeatherResponse>> GetCurrentWeatherForecast(OpenWeatherRequest request, CancellationToken cancellationToken)
        {
            return await _httpClient.GenericRequest<OpenWeatherRequest, OpenWeatherResponse>(ClientApi, string.Concat("weather?", StringExtensions
                .ParseObjectToQueryString(request, true)), cancellationToken, MethodType.Get, request);
        }
    }
}