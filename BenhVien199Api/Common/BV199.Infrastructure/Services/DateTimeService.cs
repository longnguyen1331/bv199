﻿using System;
using BV199.Application.Common.Interfaces;

namespace BV199.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
