﻿using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using BV199.Application.Common.Interfaces;
using BV199.Application.Common.Models;
using BV199.Application.ExternalServices.DrCare.Response;
using BV199.Domain.Enums;

namespace BV199.Infrastructure.Services
{
    public class DrcareService : IDrCareService
    {
        private readonly IHttpClientHandler _httpClient;

        private string ClientApi { get; } = "Drcare-api";

        public DrcareService(IHttpClientHandler httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<ServiceResult<DrCareRespone<BannerRespone>>> GetBanner(DrCareRequest request, CancellationToken cancellationToken)
        {
            return await _httpClient.GenericRequest<DrCareRequest, DrCareRespone<BannerRespone>> 
                (ClientApi, "AppSlides/paging", cancellationToken, MethodType.Get, request);
        }

        public async Task<ServiceResult<DrCareRespone<ServiceTypeRespone>>> GetServiceType(DrCareRequest request, CancellationToken cancellationToken)
        {
            return await _httpClient.GenericRequest<DrCareRequest, DrCareRespone<ServiceTypeRespone>>
                (ClientApi, "ServiceTypes/paging", cancellationToken, MethodType.Get, request);
        }
        public async Task<ServiceResult<DrCareRespone<DoctorRespone>>> GetDoctor(DrCareRequest request, CancellationToken cancellationToken)
        {
            return await _httpClient.GenericRequest<DrCareRequest, DrCareRespone<DoctorRespone>>
                (ClientApi, "Doctors/paging", cancellationToken, MethodType.Get, request);
        }
        public async Task<ServiceResult<DrCareRespone<TopServiceRespone>>> GetTopService(DrCareRequest request, CancellationToken cancellationToken)
        {
            return await _httpClient.GenericRequest<DrCareRequest, DrCareRespone<TopServiceRespone>>
                (ClientApi, "Dashboards/mobile/topservice", cancellationToken, MethodType.Get, request);
        }

        public async Task<ServiceResult<DrCareRespone<MenuWebInfoRespone>>> GetMenu(DrCareRequest request, CancellationToken cancellationToken)
        {
            return await _httpClient.GenericRequest<DrCareRequest, DrCareRespone<MenuWebInfoRespone>>
                (ClientApi, "WebInfo/webMenu", cancellationToken, MethodType.Get, request);
        }
    }
}