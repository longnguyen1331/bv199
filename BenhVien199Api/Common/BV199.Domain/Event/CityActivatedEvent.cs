﻿using BV199.Domain.Common;
using BV199.Domain.Entities;

namespace BV199.Domain.Event
{
    public class CityActivatedEvent : DomainEvent
    {
        public CityActivatedEvent(City city)
        {
            City = city;
        }

        public City City { get; }
    }
}
