﻿using BV199.Domain.Common;
using BV199.Domain.Entities;

namespace BV199.Domain.Event
{
    public class CityCreatedEvent : DomainEvent
    {
        public CityCreatedEvent(City city)
        {
            City = city;
        }

        public City City { get; }
    }
}
