﻿using System.Buffers.Text;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using Newtonsoft.Json;

namespace BV199.Application.ExternalServices.DrCare.Response 
{
    public class DrCareRequest
    {
        public string token { get; set; }
        public int pageSize { get; set; }   
        public int pageNumber { get; set; }
    }

    public class BaseRespone
    {
        public string id { get; set; }
        public int numberOrder { get; set; }
        public string clinicId { get; set; }
    }


    public class BannerRespone  : BaseRespone
    {
        public string title { get; set; }
        public bool isVisibled { get; set; }
        public int slideType { get; set; }
        public string imageUrl { get; set; }
        public string postID { get; set; }
    }

    public class ServiceTypeRespone : BaseRespone
    {
        public string name { get; set; }
        public string image { get; set; }
    }

    public class DoctorRespone : BaseRespone
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string gender { get; set; }
        public string avatar { get; set; }
        public bool isActive { get; set; }
        public int totalServices { get; set; }
        public long dob { get; set; }
        public string phone { get; set; }
        public Address address { set; get; }
        public Information information { set; get; }
        public List<Departments> departments { set; get; }
        //public List<Service> services { set; get; }
    }

    public class TopServiceRespone : BaseRespone
    {
        public int minimunAge { get; set; }
        public int maxAge { get; set; }
        public int numberOfConsult { get; set; }
        public int numberOfTests { get; set; }
        public string name { get; set; }
        public string note { get; set; }
        public string consultNote { get; set; }
        public string testsNote { get; set; }
        public string description { get; set; }
        public bool isHighLight { get; set; }
        public bool isActive { get; set; }
        public int views { get; set; }
        public string code { get; set; }
        public string image { get; set; }
        public float oldCharges { get; set; }
        public float charges { get; set; }
        public string gender { get; set; }
        public string serviceTypeId { get; set; }
    }


    public class MenuWebInfoRespone : BaseRespone
    {
        public bool isDeleted { set; get; }
        public string name { set; get; }
        public string menuType { set; get; }
        public string title { set; get; }
        public string slug { set; get; }
        public string descriptions { set; get; }
        public string keyword { set; get; }
        public string menuImage { set; get; }
        public string parentMenu { set; get; }
        public string destinationUrl { set; get; }
    }


    public class DrCareRespone<T>
    {
        public List<T> data { set; get; }
        public int totalCount { get; set; }
        public int dataCount { get; set; }
        public int pageSize { get; set; }
        public int pageNumber { get; set; }
        public int status { get; set; }
        public string message { get; set; }
    }

    public class Address
    {
        public string countryId { set; get; }
        public string country { set; get; }
        public string provinceId { set; get; }
        public string province { set; get; }
        public string districtId { set; get; }
        public string district { set; get; }
        public string address { set; get; }
    }
    public class Information
    {
        public int numberOrder { set; get; }
        public string biography { set; get; }
        public string skills { set; get; }
        public string profile { set; get; }
    }
    public class Departments
    {
        public string departmentId { set; get; }
        public Department department { set; get; }
    }
    public class Department
    {
        public string id { set; get; }
        public string name { set; get; }
        public string description { set; get; }
        public string imageUrl { set; get; }
        public string clinicId { set; get; }
    }

    public enum MenuType
    {
        News = 1,
        ListOfNews = 2,
        Departments = 3,
        Introduce = 4,
        NewDetail = 5,
        Services = 6,
        ServicePack = 7,
        Specialist = 8,
        Video = 9,
        VideoCategory = 10,
        Contact = 11,
        Url = 12,
    }
}