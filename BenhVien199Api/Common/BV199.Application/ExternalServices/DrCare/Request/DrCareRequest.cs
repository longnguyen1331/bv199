﻿namespace BV199.Application.ExternalServices.DrCare.Request
{
    public class DrCare
    {
        public string Q { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public int Id { get; set; }
    }
}