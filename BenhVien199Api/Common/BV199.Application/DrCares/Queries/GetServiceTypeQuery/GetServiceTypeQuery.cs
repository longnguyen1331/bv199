﻿using System.Threading;
using System.Threading.Tasks;
using BV199.Application.Common.Interfaces;
using BV199.Application.Common.Models;
using BV199.Application.Dto;
using BV199.Application.ExternalServices.DrCare.Response;
using BV199.Application.ExternalServices.OpenWeather.Request;
using MapsterMapper;

namespace BV199.Application.WeatherForecasts.Queries.GetServiceTypeQuery
{
    public class GetServiceTypeQuery : IRequestWrapper<DrCareRespone<ServiceTypeRespone>>
    {
        public string token { get; set; }
    }

    public class GetServiceTypeQueryHandler : IRequestHandlerWrapper<GetServiceTypeQuery, DrCareRespone<ServiceTypeRespone>>
    {
        private readonly IDrCareService _drCareService;
        private readonly IMapper _mapper;

        public GetServiceTypeQueryHandler(IDrCareService drCareService, IMapper mapper)
        {
            _drCareService = drCareService;
            _mapper = mapper;
        }

        public async Task<ServiceResult<DrCareRespone<ServiceTypeRespone>>> Handle(GetServiceTypeQuery request, CancellationToken cancellationToken)
        {
            var drCareRequest = _mapper.Map<DrCareRequest>(request);
            var result = await _drCareService.GetServiceType(drCareRequest, cancellationToken);

            return result.Succeeded
                ? ServiceResult.Success(_mapper.Map<DrCareRespone<ServiceTypeRespone>>(result.Data))
                : ServiceResult.Failed<DrCareRespone<ServiceTypeRespone>>(ServiceError.ServiceProvider);
        }
    }
}