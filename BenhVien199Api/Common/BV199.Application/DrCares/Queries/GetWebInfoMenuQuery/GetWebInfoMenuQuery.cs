﻿using System.Threading;
using System.Threading.Tasks;
using BV199.Application.Common.Interfaces;
using BV199.Application.Common.Models;
using BV199.Application.Dto;
using BV199.Application.ExternalServices.DrCare.Response;
using BV199.Application.ExternalServices.OpenWeather.Request;
using MapsterMapper;

namespace BV199.Application.WeatherForecasts.Queries.GetWebInfoMenuQuery
{
    public class GetWebInfoMenuQuery : IRequestWrapper<DrCareRespone<MenuWebInfoRespone>>
    {
        public string token { get; set; }
    }

    public class GetWebInfoMenuQueryHandler : IRequestHandlerWrapper<GetWebInfoMenuQuery, DrCareRespone<MenuWebInfoRespone>>
    {
        private readonly IDrCareService _drCareService;
        private readonly IMapper _mapper;

        public GetWebInfoMenuQueryHandler(IDrCareService drCareService, IMapper mapper)
        {
            _drCareService = drCareService;
            _mapper = mapper;
        }

        public async Task<ServiceResult<DrCareRespone<MenuWebInfoRespone>>> Handle(GetWebInfoMenuQuery request, CancellationToken cancellationToken)
        {
            var drCareRequest = _mapper.Map<DrCareRequest>(request);
            var result = await _drCareService.GetMenu(drCareRequest, cancellationToken);

            return result.Succeeded
                ? ServiceResult.Success(_mapper.Map<DrCareRespone<MenuWebInfoRespone>>(result.Data))
                : ServiceResult.Failed<DrCareRespone<MenuWebInfoRespone>>(ServiceError.ServiceProvider);
        }
    }
}