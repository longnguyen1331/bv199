﻿using System.Threading;
using System.Threading.Tasks;
using BV199.Application.Common.Interfaces;
using BV199.Application.Common.Models;
using BV199.Application.Dto;
using BV199.Application.ExternalServices.DrCare.Response;
using BV199.Application.ExternalServices.OpenWeather.Request;
using MapsterMapper;

namespace BV199.Application.WeatherForecasts.Queries.GetCurrentBannerQuery
{
    public class GetCurrentBannerQuery : IRequestWrapper<DrCareRespone<BannerRespone>>
    {
        public string token { get; set; }
    }

    public class GetCurrentBannerQueryHandler : IRequestHandlerWrapper<GetCurrentBannerQuery, DrCareRespone<BannerRespone>>
    {
        private readonly IDrCareService _drCareService;
        private readonly IMapper _mapper;

        public GetCurrentBannerQueryHandler(IDrCareService drCareService, IMapper mapper)
        {
            _drCareService = drCareService;
            _mapper = mapper;
        }

        public async Task<ServiceResult<DrCareRespone<BannerRespone>>> Handle(GetCurrentBannerQuery request, CancellationToken cancellationToken)
        {
            var drCareRequest = _mapper.Map<DrCareRequest>(request);
            var result = await _drCareService.GetBanner(drCareRequest, cancellationToken);

            return result.Succeeded
                ? ServiceResult.Success(_mapper.Map<DrCareRespone<BannerRespone>>(result.Data))
                : ServiceResult.Failed<DrCareRespone<BannerRespone>>(ServiceError.ServiceProvider);
        }
    }
}