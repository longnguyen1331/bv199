﻿using System.Threading;
using System.Threading.Tasks;
using BV199.Application.Common.Interfaces;
using BV199.Application.Common.Models;
using BV199.Application.Dto;
using BV199.Application.ExternalServices.DrCare.Response;
using BV199.Application.ExternalServices.OpenWeather.Request;
using MapsterMapper;

namespace BV199.Application.WeatherForecasts.Queries.GetDoctorQuery
{
    public class GetDoctorQuery : IRequestWrapper<DrCareRespone<DoctorRespone>>
    {
        public string token { get; set; }
    }

    public class GetDoctorQueryHandler : IRequestHandlerWrapper<GetDoctorQuery, DrCareRespone<DoctorRespone>>
    {
        private readonly IDrCareService _drCareService;
        private readonly IMapper _mapper;

        public GetDoctorQueryHandler(IDrCareService drCareService, IMapper mapper)
        {
            _drCareService = drCareService;
            _mapper = mapper;
        }

        public async Task<ServiceResult<DrCareRespone<DoctorRespone>>> Handle(GetDoctorQuery request, CancellationToken cancellationToken)
        {
            var drCareRequest = _mapper.Map<DrCareRequest>(request);
            var result = await _drCareService.GetDoctor(drCareRequest, cancellationToken);

            return result.Succeeded
                ? ServiceResult.Success(_mapper.Map<DrCareRespone<DoctorRespone>>(result.Data))
                : ServiceResult.Failed<DrCareRespone<DoctorRespone>>(ServiceError.ServiceProvider);
        }
    }
}