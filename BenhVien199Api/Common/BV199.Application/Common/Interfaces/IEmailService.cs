﻿using System.Threading.Tasks;
using BV199.Application.Common.Models;

namespace BV199.Application.Common.Interfaces
{
    public interface IEmailService
    {
        Task SendAsync(EmailRequest request);
    }
}
