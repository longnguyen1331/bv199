﻿using System.Threading.Tasks;
using BV199.Domain.Common;

namespace BV199.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
