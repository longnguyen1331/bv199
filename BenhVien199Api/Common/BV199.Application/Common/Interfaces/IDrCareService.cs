﻿using BV199.Application.Common.Models;
using BV199.Application.ExternalServices.OpenWeather.Request;
using BV199.Application.ExternalServices.OpenWeather.Response;
using System.Threading.Tasks;
using System.Threading;
using BV199.Application.ExternalServices.DrCare.Response;

namespace BV199.Application.Common.Interfaces
{
    public interface IDrCareService
    {
        Task<ServiceResult<DrCareRespone<BannerRespone>>> GetBanner(DrCareRequest rq, CancellationToken cancellationToken);
        Task<ServiceResult<DrCareRespone<ServiceTypeRespone>>> GetServiceType(DrCareRequest rq, CancellationToken cancellationToken);
        Task<ServiceResult<DrCareRespone<DoctorRespone>>> GetDoctor(DrCareRequest rq, CancellationToken cancellationToken);
        Task<ServiceResult<DrCareRespone<TopServiceRespone>>> GetTopService(DrCareRequest rq, CancellationToken cancellationToken);
        Task<ServiceResult<DrCareRespone<MenuWebInfoRespone>>> GetMenu(DrCareRequest rq, CancellationToken cancellationToken);
    }
}
