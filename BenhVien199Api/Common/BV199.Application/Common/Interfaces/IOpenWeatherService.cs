﻿using System.Threading;
using System.Threading.Tasks;
using BV199.Application.Common.Models;
using BV199.Application.ExternalServices.OpenWeather.Request;
using BV199.Application.ExternalServices.OpenWeather.Response;

namespace BV199.Application.Common.Interfaces
{
    public interface IOpenWeatherService
    {
        Task<ServiceResult<OpenWeatherResponse>> GetCurrentWeatherForecast(OpenWeatherRequest request,
            CancellationToken cancellationToken);
    }
   
}