﻿using System.Collections.Generic;
using BV199.Application.Dto;

namespace BV199.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildDistrictsFile(IEnumerable<DistrictDto> districts);
    }
}
